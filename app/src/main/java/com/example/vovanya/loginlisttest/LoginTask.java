package com.example.vovanya.loginlisttest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.widget.Toast;

class LoginTask extends AsyncTask<String, Void, Boolean> {

    public static final String USERNAME = "test";
    public static final String PASSWORD = "7777777";

    private Context context;
    private ProgressDialog progressDialog;

    public LoginTask(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.show();
    }

    @Override
    protected Boolean doInBackground(String... params) {

        //imitating connection
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return (params[0].equals(USERNAME) && params[1].equals(PASSWORD));
    }

    @Override
    protected void onPostExecute(Boolean result) {
        super.onPostExecute(result);
        progressDialog.cancel();
        if (result) {
            context.startActivity(new Intent(context, ListActivity.class));
            ((Activity) context).finish();
        } else {
            Toast.makeText(context, R.string.incorrect_username_password, Toast.LENGTH_LONG).show();
        }

    }
}