package com.example.vovanya.loginlisttest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {



    EditText usernameEditText, passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        usernameEditText = (EditText) findViewById(R.id.username_edit);
        passwordEditText = (EditText) findViewById(R.id.password_edit);

        findViewById(R.id.button_login).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_login:
                String username = usernameEditText.getText().toString();
                String password = passwordEditText.getText().toString();
                if (password.length() > 0 && username.length() > 0) {
                    new LoginTask(this).execute(username, password);
                } else {
                    Toast.makeText(this, R.string.fill_all_fields, Toast.LENGTH_LONG).show();
                }
                break;
        }
    }





}
